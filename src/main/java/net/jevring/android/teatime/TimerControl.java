/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;


/**
 * The sole purpose of this activity is so that we can display a dialog box from a service.
 * We wouldn't have to do this if Gingerbread could handle clickable remote views items in
 * extended notifications.
 *
 * @author markus@jevring.net
 */
public class TimerControl extends Activity {
	public static final String ACTION_CONTROL_TIMER = "ControlTimer";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		Bundle extras = i.getExtras();
		final int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		final int timerId = extras.getInt(TeaTimerService.EXTRA_TIMER_ID);
		final boolean paused = extras.getBoolean("paused", false);
		// if the timer finishes while we wait, that's fine
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.gb_timer_action_choice);
		builder.setIcon(R.drawable.kteatime);
		builder.setCancelable(true);
		builder.setNegativeButton("Stop", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				startService(newIntent(TeaTimerService.ACTION_STOP_TIMER, widgetId, timerId));
				finish();
			}
		});
		if (paused) {
			builder.setPositiveButton("Resume", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialogInterface, int i) {
					startService(newIntent(TeaTimerService.ACTION_RESUME_TIMER, widgetId, timerId));
					finish();
				}
			});
		} else {
			builder.setPositiveButton("Pause", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialogInterface, int i) {
					startService(newIntent(TeaTimerService.ACTION_PAUSE_TIMER, widgetId, timerId));
					finish();
				}
			});
		}
		builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialogInterface) {
				finish();
			}
		});
		builder.show();
	}

	private Intent newIntent(String action, int appWidgetId, int timerId) {
		Intent i = new Intent(this, TeaTimerService.class);
		i.setAction(action);
		i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		i.putExtra(TeaTimerService.EXTRA_TIMER_ID, timerId);
		return i;
	}
}