/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * The running state tracks when countdowns were started for a timer.
 * This is important as services can be killed. If we're killed, we ask that
 * the same intent used last time be called to start it. When we do, we have
 * to restart ALL previously running timers.
 *
 * @author markus@jevring.net
 */
public class RunningState {
	private static final String RUNNING_STATE = "RunningState";
	private final SharedPreferences runningState;
	private final SharedPreferences.Editor editableRunningState;

	public RunningState(Context context) {
		runningState = context.getSharedPreferences(RUNNING_STATE, Context.MODE_PRIVATE);
		editableRunningState = runningState.edit();
	}

	public void remove(int timerId) {
		// cleaning up running state
		editableRunningState.remove(String.valueOf(timerId));
		editableRunningState.commit();
	}

	public void start(int timerId) {
		// store when we started
		editableRunningState.putLong(String.valueOf(timerId), System.currentTimeMillis());
		editableRunningState.commit();
	}

	public long getStartingTime(String timerId) {
		return runningState.getLong(timerId, -1);
	}

	public Set<String> getTimers() {
		return runningState.getAll().keySet();
	}
}
