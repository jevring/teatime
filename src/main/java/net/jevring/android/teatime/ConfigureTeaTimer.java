/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


/**
 * Configures the tea timer.
 *
 * @author markus@jevring.net
 */
public class ConfigureTeaTimer extends Activity {
	public static final int SELECT_RINGTONE = 1;
	// it's fine to have instance parameters here, as only one widget can be configured at the same time
	private int widgetId;
	private EditText teaName;
	private EditText duration;
	private CheckBox vibrateOnStart;
	private CheckBox vibrateOnKitchenTime;
	private CheckBox vibrateAtEnd;
	private CheckBox soundAtEnd;
	private Uri selectedRingtoneUri;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// if the user clicks back of home or something, we shouldn't get a ghost widget
		setResult(RESULT_CANCELED);
		// show the configuration gui on screen
		setContentView(R.layout.configure);

		teaName = (EditText) findViewById(R.id.tea_name);
		duration = (EditText) findViewById(R.id.duration);
		Button ok = (Button) findViewById(R.id.duration_set_ok);

		vibrateOnStart = (CheckBox) findViewById(R.id.vibrate_on_start);
		vibrateOnKitchenTime = (CheckBox) findViewById(R.id.vibrate_on_kitchen_time);
		vibrateAtEnd = (CheckBox) findViewById(R.id.vibrate_at_end);
		soundAtEnd = (CheckBox) findViewById(R.id.sound_at_end);
		Button chooseRingTone = (Button) findViewById(R.id.select_sound);

		chooseRingTone.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,
				                RingtoneManager.TYPE_ALARM | RingtoneManager.TYPE_NOTIFICATION);
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Choose sound");
				// nothing I try here will provide a default that is actually the default...
				//intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
				//intent.putExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
				startActivityForResult(intent, SELECT_RINGTONE);
			}
		});


		// find the widget id.
		final Intent intent = getIntent();
		final Bundle extras = intent.getExtras();
		if (extras != null) {
			widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
				// we were launched without being connected to a widget, just end it.
				finish();
			} else {
				ok.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							int duration = Integer.parseInt(ConfigureTeaTimer.this.duration.getText().toString());

							if (duration < 0) {
								Toast.makeText(ConfigureTeaTimer.this, "Duration must be > 0", Toast.LENGTH_LONG).show();
								return;
							}

							TimerPreferences.store(ConfigureTeaTimer.this,
							                       widgetId,
							                       teaName.getText().toString(),
							                       duration,
							                       vibrateOnStart.isChecked(),
							                       vibrateOnKitchenTime.isChecked(),
							                       vibrateAtEnd.isChecked(),
							                       soundAtEnd.isChecked(),
							                       selectedRingtoneUri);

							// it's unfortunate that we have to do this here. 
							// it would have been nicer if we were able to do
							// it in the widget itself, but apparently that's not sufficient
							TeaTimer.initializeWidget(widgetId, ConfigureTeaTimer.this);

							// tell the OS we're done with the configuration
							Intent result = new Intent();
							result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
							setResult(Activity.RESULT_OK, result);
							finish();
						} catch (NumberFormatException e) {
							Toast.makeText(ConfigureTeaTimer.this,
							               duration.getText().toString() + " is not a number",
							               Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		} else if (Intent.ACTION_CREATE_SHORTCUT.equals(intent.getAction())) {
			ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						int duration = Integer.parseInt(ConfigureTeaTimer.this.duration.getText().toString());

						if (duration <= 0) {
							Toast.makeText(ConfigureTeaTimer.this, "Duration must be > 0", Toast.LENGTH_LONG).show();
							return;
						}

						// proxy this via the StartTimer activity
						Intent launchIntent = new Intent(ConfigureTeaTimer.this, StartTimer.class);
						launchIntent.putExtra(TeaTimerService.EXTRA_TEA_NAME, teaName.getText().toString());
						launchIntent.putExtra(TeaTimerService.EXTRA_DURATION, duration);
						launchIntent.putExtra(TeaTimerService.EXTRA_VIBRATE_ON_START, vibrateOnStart.isChecked());
						launchIntent.putExtra(TeaTimerService.EXTRA_VIBRATE_ON_KITCHEN_TIME, vibrateOnKitchenTime.isChecked());
						launchIntent.putExtra(TeaTimerService.EXTRA_VIBRATE_AT_END, vibrateAtEnd.isChecked());
						launchIntent.putExtra(TeaTimerService.EXTRA_SOUND_AT_END, soundAtEnd.isChecked());
						if (selectedRingtoneUri != null) {
							launchIntent.putExtra(TeaTimerService.EXTRA_RINGTONE, selectedRingtoneUri.toString());
							Log.i(TeaTimerService.TAG, "Selected ringtone uri: " + selectedRingtoneUri.toString());
						}
						launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

						Intent shortcutIntent = new Intent();
						shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, launchIntent);
						shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, teaName.getText().toString());
						//shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, "name");
						// todo: the image is too large, and/or isn't centered correctly
						shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
						                        Intent.ShortcutIconResource.fromContext(ConfigureTeaTimer.this,
						                                                                R.drawable.kteatime));
						setResult(Activity.RESULT_OK, shortcutIntent);
						finish();
					} catch (NumberFormatException e) {
						Toast.makeText(ConfigureTeaTimer.this,
						               duration.getText().toString() + " is not a number",
						               Toast.LENGTH_LONG).show();
					}
				}
			});

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SELECT_RINGTONE) {
			if (resultCode == RESULT_OK) {
				selectedRingtoneUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
			}
		}
	}

}