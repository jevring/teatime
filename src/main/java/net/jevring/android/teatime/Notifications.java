/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

/**
 * @author markus@jevring.net
 */
public class Notifications extends BroadcastReceiver {
	public static final String TAG = TeaTimerService.TAG;
	public static final String ACTION_KITCHEN_TIME = "KitchenTimeVibration";
	public static final String ACTION_END_NOTIFICATION = "EndNotification";
	public static final long[] VIBRATION_PATTERN = new long[]{0, 100, 200, 100, 200, 100};

	@Override
	public void onReceive(Context context, Intent intent) {
		final Bundle extras = intent.getExtras();
		if (extras == null) {
			throw new IllegalArgumentException("Must provide extras with '" + TeaTimerService.EXTRA_TIMER_ID + "' set");
		}
		final int timerId = extras.getInt(TeaTimerService.EXTRA_TIMER_ID);

		if (ACTION_KITCHEN_TIME.equals(intent.getAction())) {
			goToTheKitchen(context);
		} else if (ACTION_END_NOTIFICATION.equals(intent.getAction())) {
			try {
				String teaName = extras.getString(TeaTimerService.EXTRA_TEA_NAME);
				boolean soundAtEnd = extras.getBoolean(TeaTimerService.EXTRA_SOUND_AT_END);
				boolean vibrateAtEnd = extras.getBoolean(TeaTimerService.EXTRA_VIBRATE_AT_END);
				int currentDuration = extras.getInt(TeaTimerService.EXTRA_DURATION);
				Uri ringtone = null;
				String ringtoneUriAsString = extras.getString(TeaTimerService.EXTRA_RINGTONE);
				if (ringtoneUriAsString != null) {
					ringtone = Uri.parse(ringtoneUriAsString);
				}
				Log.d(TAG, "Tea ready: " + teaName);

				teaReady(context, timerId, teaName, soundAtEnd, ringtone, vibrateAtEnd);

				int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
				if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
					// reset the widget
					TeaTimer.updateWidget(context, teaName, currentDuration, currentDuration, widgetId, false, false);
				}
			} catch (NoSuchTimerException e) {
				Log.e(TAG, "Unknown timer", e);
			}
		}
	}

	private void goToTheKitchen(Context context) {
		final Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(VIBRATION_PATTERN, -1);
		// todo: change this to use the name of the timer?
		Log.i(TAG, context.getString(R.string.time_to_go_to_the_kitchen));
		Toast.makeText(context, R.string.time_to_go_to_the_kitchen, Toast.LENGTH_LONG).show();
	}

	private void teaReady(Context context,
	                      int timerId,
	                      String teaName,
	                      boolean soundAtEnd,
	                      Uri ringtoneUri,
	                      boolean vibrateAtEnd) throws NoSuchTimerException {
		String time = DateFormat.getTimeInstance().format(new Date());
		String contentTitle = teaName + " ready at " + time;
		String message = "Your " + teaName + " is ready";
		NotificationManager notificationService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// no need to have the creation time for the notification here, as we won't be updating it
		Notification notification = new Notification(android.R.drawable.ic_dialog_info, message, System.currentTimeMillis());
		if (soundAtEnd) {
			// do this here, rather than outside the counter, so that we can handle the case where the user
			// changes the default notification while we're ticking down.
			final Uri notificationUri;
			if (ringtoneUri == null) {
				notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			} else {
				notificationUri = ringtoneUri;
			}

			final Ringtone ringtone = RingtoneManager.getRingtone(context, notificationUri);
			if (ringtone != null) {
				// can be null in the emulator
				//notification.sound = notificationUri;
				// as notifications can be arbitrarily delayed, it would seem that simply calling things is better.
				// todo: this means that, if the sound loops (why this is a property of the SOUND, rather than the player, I will never know), it'll be stuck like this forever
				ringtone.play();
			}
		}

		if (vibrateAtEnd) {
			//notification.vibrate = VIBRATION_PATTERN;
			// as notifications can be arbitrarily delayed, it would seem that simply calling things is better.
			final Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATION_PATTERN, -1);
		}

		Intent intent = newIntent(context, TeaTimerService.ACTION_REMOVE_NOTIFICATION, timerId);
		PendingIntent removeNotification = PendingIntent.getService(context, timerId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(context, contentTitle, context.getString(R.string.click_to_dismiss), removeNotification);
		notificationService.notify(timerId, notification);
		// remove the equivalent ongoing notification
		notificationService.cancel(TeaTimerService.ONGOING_NOTIFICATION_OFFSET + timerId);
	}

	private Intent newIntent(Context context, String action, int timerId) {
		Intent i = new Intent(context, TeaTimerService.class);
		i.setAction(action);
		i.putExtra(TeaTimerService.EXTRA_TIMER_ID, timerId);
		return i;
	}
}
