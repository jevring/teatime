/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.content.Context;
import android.content.SharedPreferences;

import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains the timer preferences. This is a wrapper around {@link android.content.SharedPreferences}.
 * Use the {@link #get(Context, int) factory method} to get the preferences. This will check if they are already
 * loaded or not. As android can throw stuff ouf of memory at its convenience, things that might
 * have been cached a second ago might not be cached now.
 *
 * @author markus@jevring.net
 */
public class TimerPreferences {
	private static final Map<Integer, TimerPreferences> preferences = new HashMap<Integer, TimerPreferences>();
	public static final String PACKAGE = "net.jevring.android.teatime.";
	public static final String DURATION = "duration";
	public static final String TEA_NAME = "tea_name";
	public static final String VIBRATE_AT_END = "vibrate_at_end";
	public static final String VIBRATE_ON_START = "vibrate_on_start";
	public static final String VIBRATE_ON_KITCHEN_TIME = "vibrate_on_kitchen_time";
	public static final String SOUND_AT_END = "sound_at_end";
	public static final String RINGTONE = "ringtone";
	private final String teaName;
	private final int duration;
	private final boolean vibrateAtEnd;
	private final boolean vibrateOnStart;
	private final boolean vibrateOnKitchenTime;
	private final boolean soundAtEnd;
	private final Uri ringtone;

	private TimerPreferences(Context context, int widgetId) throws NoSuchTimerException {
		final SharedPreferences preferences = context.getSharedPreferences(PACKAGE + widgetId, Context.MODE_PRIVATE);
		duration = preferences.getInt(DURATION, -1);
		if (duration == -1) {
			throw new NoSuchTimerException(widgetId);
		}
		teaName = preferences.getString(TEA_NAME, context.getString(R.string.tea_name_preset));
		vibrateAtEnd = preferences.getBoolean(VIBRATE_AT_END, false);
		vibrateOnStart = preferences.getBoolean(VIBRATE_ON_START, false);
		vibrateOnKitchenTime = preferences.getBoolean(VIBRATE_ON_KITCHEN_TIME, false);
		soundAtEnd = preferences.getBoolean(SOUND_AT_END, false);
		String ringtoneUri = preferences.getString(RINGTONE, null);
		if (ringtoneUri == null) {
			ringtone = null;
		} else {
			ringtone = Uri.parse(ringtoneUri);	
		}
	}

	public static synchronized TimerPreferences get(Context context, int widgetId) throws NoSuchTimerException {
		TimerPreferences p = preferences.get(widgetId);
		if (p == null) {
			p = new TimerPreferences(context, widgetId);
			preferences.put(widgetId, p);
		}
		return p;
	}

	public static TimerPreferences store(Context context,
	                                     int widgetId,
	                                     String teaName,
	                                     int duration,
	                                     boolean vibrateOnStart,
	                                     boolean vibrateOnKitchenTime,
	                                     boolean vibrateAtEnd,
	                                     boolean soundAtEnd, Uri selectedRingtoneUri) {
		final SharedPreferences preferences = context.getSharedPreferences(PACKAGE + widgetId, Context.MODE_PRIVATE);
		final SharedPreferences.Editor p = preferences.edit();
		p.putString(TEA_NAME, teaName);
		p.putInt(DURATION, duration);
		p.putBoolean(VIBRATE_ON_START, vibrateOnStart);
		p.putBoolean(VIBRATE_ON_KITCHEN_TIME, vibrateOnKitchenTime);
		p.putBoolean(VIBRATE_AT_END, vibrateAtEnd);
		p.putBoolean(SOUND_AT_END, soundAtEnd);
		if (selectedRingtoneUri != null) {
			p.putString(RINGTONE, selectedRingtoneUri.toString());
		}
		p.commit();
		try {
			return get(context, widgetId);
		} catch (NoSuchTimerException e) {
			// can never happen, as we just wrote it to disk
			throw new AssertionError(e);
		}
	}

	public static TimerPreferences delete(Context context, int widgetId) {
		// ensure that we're not leaving garbage behind
		context.getSharedPreferences(TimerPreferences.PACKAGE + widgetId, Context.MODE_PRIVATE).edit().clear().commit();
		return preferences.remove(widgetId);
	}

	public String getTeaName() {
		return teaName;
	}

	public int getDuration() {
		return duration;
	}

	public boolean isVibrateAtEnd() {
		return vibrateAtEnd;
	}

	public boolean isVibrateOnStart() {
		return vibrateOnStart;
	}

	public boolean isVibrateOnKitchenTime() {
		return vibrateOnKitchenTime;
	}

	public boolean isSoundAtEnd() {
		return soundAtEnd;
	}

	public Uri getRingtone() {
		return ringtone;
	}

	@Override
	public String toString() {
		return "TimerPreferences{" +
				"teaName='" + teaName + '\'' +
				", duration=" + duration +
				", vibrateAtEnd=" + vibrateAtEnd +
				", vibrateOnStart=" + vibrateOnStart +
				", vibrateOnKitchenTime=" + vibrateOnKitchenTime +
				", soundAtEnd=" + soundAtEnd +
				'}';
	}
}
