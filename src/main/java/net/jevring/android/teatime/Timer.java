/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.util.Log;

/**
 * @author markus@jevring.net
 */
public class Timer {
	public static final String TAG = TeaTimerService.TAG;
	private final long creation = System.currentTimeMillis();
	private final TeaTimerService service;
	private final AlarmManager alarmManager;
	private final Vibrator vibrator;
	private final int id;
	private final Context context;
	private CountDownTimer countDownTimer;
	private int remainingDuration;
	private volatile boolean paused = false;
	private final PendingIntent triggerGoToKitchenVibration;
	private final PendingIntent endNotification;

	private final int duration;
	private final String teaName;
	private final boolean vibrateOnStart;
	private final boolean vibrateOnKitchenTime;
	/**
	 * Will be {@link AppWidgetManager#INVALID_APPWIDGET_ID} if this timer isn't for a widget
	 */
	private final int widgetId;

	private Timer(int id,
	              TeaTimerService service,
	              int duration,
	              String teaName,
	              boolean vibrateOnStart,
	              boolean vibrateOnKitchenTime,
	              boolean soundAtEnd,
	              boolean vibrateAtEnd,
	              Uri ringtoneUri,
	              int widgetId) {
		this.service = service;
		this.id = id;
		this.context = service;
		this.duration = duration;
		this.remainingDuration = duration;
		this.teaName = teaName;
		this.vibrateOnStart = vibrateOnStart;
		this.vibrateOnKitchenTime = vibrateOnKitchenTime;
		this.widgetId = widgetId;
		this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		this.vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

		Intent kitchenTimeIntent = newIntent(Notifications.ACTION_KITCHEN_TIME);
		this.triggerGoToKitchenVibration =
				PendingIntent.getBroadcast(context, id, kitchenTimeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		Intent endNotificationIntent = newIntent(Notifications.ACTION_END_NOTIFICATION);
		endNotificationIntent.putExtra(TeaTimerService.EXTRA_TEA_NAME, teaName);
		endNotificationIntent.putExtra(TeaTimerService.EXTRA_SOUND_AT_END, soundAtEnd);
		endNotificationIntent.putExtra(TeaTimerService.EXTRA_VIBRATE_AT_END, vibrateAtEnd);
		endNotificationIntent.putExtra(TeaTimerService.EXTRA_DURATION, duration);
		endNotificationIntent.putExtra(TeaTimerService.EXTRA_RINGTONE, ringtoneUri == null ? null : ringtoneUri.toString());
		this.endNotification = PendingIntent.getBroadcast(context, id, endNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public Timer(int id,
	             TeaTimerService service,
	             int duration,
	             String teaName,
	             boolean vibrateOnStart,
	             boolean vibrateOnKitchenTime,
	             boolean soundAtEnd,
	             boolean vibrateAtEnd,
	             Uri ringtoneUri) {
		this(id,
		     service,
		     duration,
		     teaName,
		     vibrateOnStart,
		     vibrateOnKitchenTime,
		     soundAtEnd,
		     vibrateAtEnd,
		     ringtoneUri,
		     AppWidgetManager.INVALID_APPWIDGET_ID);
	}

	public Timer(int id, TeaTimerService service, TimerPreferences timerPreferences, int widgetId) {
		this(id,
		     service,
		     timerPreferences.getDuration(),
		     timerPreferences.getTeaName(),
		     timerPreferences.isVibrateOnStart(),
		     timerPreferences.isVibrateOnKitchenTime(),
		     timerPreferences.isSoundAtEnd(),
		     timerPreferences.isVibrateAtEnd(),
		     timerPreferences.getRingtone(),
		     widgetId);
	}


	public Intent newIntent(String action) {
		Intent i = new Intent(context, Notifications.class);
		i.setAction(action);
		i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
		i.putExtra(TeaTimerService.EXTRA_TIMER_ID, id);
		return i;
	}

	private void ensureCountDownTimerAvailable() {
		if (countDownTimer == null) {
			// start with remainingDuration, in case we're restarting in the middle.
			// see setRemainingDuration()
			countDownTimer = new CountDownTimer(remainingDuration * 1000, 1000) {

				@Override
				public void onTick(long millisUntilFinished) {
					remainingDuration = (int) (millisUntilFinished / 1000) - 1;
					Log.d(TAG, "TICKING for timer '" + id + "' with remaining duration '" + remainingDuration + "'");
					TeaTimer.updateWidget(context, teaName, remainingDuration, duration, widgetId, true, false);
					service.updateOngoingNotification(Timer.this);
				}

				@Override
				public void onFinish() {
					// this is incredibly inexact. Takes like 2 seconds for the notification to show up,
					// and the alarm to go off. Going through this method "only", by comparison, takes
					// around 100-150ms
					Log.d(TAG, "FINISHED countdown for timer '" + id + "' with duration '" + duration + "'");
					service.stop(id, false);
					countDownTimer = null;
					// reset it so we start from scratch
					remainingDuration = duration;
				}
			};
		}
	}

	public synchronized void start() {
		paused = false;
		ensureCountDownTimerAvailable();
		if (vibrateOnStart) {
			vibrator.vibrate(50);
		}
		// handle kitchen vibration
		if (vibrateOnKitchenTime && remainingDuration > 20) {
			alarmManager.set(AlarmManager.RTC_WAKEUP,
			                 System.currentTimeMillis() + ((remainingDuration - 20) * 1000),
			                 triggerGoToKitchenVibration);
		}

		// handle final notification
		alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (remainingDuration * 1000), endNotification);
		countDownTimer.start();
	}

	public synchronized void stop() {
		if (countDownTimer != null) {
			countDownTimer.cancel();
			if (vibrateOnKitchenTime && remainingDuration > 20) {
				alarmManager.cancel(triggerGoToKitchenVibration);
			}
			alarmManager.cancel(endNotification);
			countDownTimer = null;
		}
	}

	public synchronized void reset() {
		TeaTimer.updateWidget(context, teaName, duration, duration, widgetId, false, false);
	}

	// actually, pause and resume are just stop and start without resetting remainingDuration in between!

	public void pause() {
		paused = true;
		stop();
		TeaTimer.updateWidget(context, teaName, remainingDuration, duration, widgetId, true, true);
	}

	public void resume() {
		start();
	}

	public boolean isPaused() {
		return paused;
	}

	public int getRemainingDuration() {
		return remainingDuration;
	}

	public void setRemainingDuration(int remainingDuration) {
		// this is useful when restarting the counters if they were killed while counting down
		this.remainingDuration = remainingDuration;
	}

	public long getCreation() {
		return creation;
	}

	public int getId() {
		return id;
	}

	public String getTeaName() {
		return teaName;
	}

	public int getDuration() {
		return duration;
	}

	public int getWidgetId() {
		return widgetId;
	}

	@Override
	public String toString() {
		return "Timer{" +
				"creation=" + creation +
				", id=" + id +
				", teaName='" + teaName + '\'' +
				", duration=" + duration +
				", remainingDuration=" + remainingDuration +
				", widgetId=" + widgetId +
				'}';
	}
}
