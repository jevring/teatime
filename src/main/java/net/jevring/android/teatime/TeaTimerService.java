/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author markus@jevring.net
 */
public class TeaTimerService extends Service {
	public static final String TAG = "TeaTimer";
	/**
	 * Keeps track of all the timers. This has to be static, as we start a new instance of this class
	 * with each intent. It would be nice if I could count on that, but I have no idea if they'll occasionally
	 * decide to reuse an existing class.
	 */
	private static final Map<Integer, Timer> timers = new ConcurrentHashMap<Integer, Timer>();
	private static final Map<Integer, Integer> widgetIdToTimerId = new ConcurrentHashMap<Integer, Integer>();
	private static final AtomicInteger timerIdGenerator = new AtomicInteger();
	public static final String ACTION_START_TIMER = TimerPreferences.PACKAGE + "StartTimer";
	public static final String ACTION_PAUSE_TIMER = TimerPreferences.PACKAGE + "PauseTimer";
	public static final String ACTION_RESUME_TIMER = TimerPreferences.PACKAGE + "ResumeTimer";
	public static final String ACTION_STOP_TIMER = TimerPreferences.PACKAGE + "StopTimer";
	public static final String ACTION_DELETE_TIMER = TimerPreferences.PACKAGE + "DeleteTimer";
	public static final String ACTION_SHOW_ACTION_DIALOG = TimerPreferences.PACKAGE + "ShowActionDialog";
	public static final String ACTION_REMOVE_NOTIFICATION = TimerPreferences.PACKAGE + "RemoveNotification";
	public static final String ACTION_DO_NOTHING = TimerPreferences.PACKAGE + "DoNothing";
	public static final String EXTRA_TIMER_ID = TimerPreferences.PACKAGE + "TimerId";
	public static final String EXTRA_TEA_NAME = TimerPreferences.PACKAGE + TimerPreferences.TEA_NAME;
	public static final String EXTRA_DURATION = TimerPreferences.PACKAGE + TimerPreferences.DURATION;
	public static final String EXTRA_RINGTONE = TimerPreferences.PACKAGE + TimerPreferences.RINGTONE;
	public static final String EXTRA_SOUND_AT_END = TimerPreferences.PACKAGE + TimerPreferences.SOUND_AT_END;
	public static final String EXTRA_VIBRATE_AT_END = TimerPreferences.PACKAGE + TimerPreferences.VIBRATE_AT_END;
	public static final String EXTRA_VIBRATE_ON_START = TimerPreferences.PACKAGE + TimerPreferences.VIBRATE_ON_START;
	public static final String EXTRA_VIBRATE_ON_KITCHEN_TIME =
			TimerPreferences.PACKAGE + TimerPreferences.VIBRATE_ON_KITCHEN_TIME;
	/**
	 * Used so that the "completed" notifications don't clash with the ongoing ones.
	 */
	protected static final int ONGOING_NOTIFICATION_OFFSET = Integer.MAX_VALUE / 2;

	private final Object runningStateSetterLock = new Object();
	private RunningState runningState;

	private volatile int lastStartId;

	private PendingIntent newPendingIntent(String action, Timer timer) {
		Intent intent = new Intent(this, TeaTimerService.class);
		intent.setAction(action);
		intent.putExtra(EXTRA_TIMER_ID, timer.getId());
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, timer.getWidgetId());
		return PendingIntent.getService(this, timer.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// don't allow binding, return null
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// as much as I'd like to set this where I declare it, we end up getting an exception
		// if we do, deep down inside android...

		synchronized (runningStateSetterLock) {
			if (runningState == null) {
				runningState = new RunningState(this);
			}
		}
		Log.d(TAG, "TeaTimerService.onStartCommand: startId = " + startId);
		lastStartId = startId;
		handleIntent(intent);
		return Service.START_STICKY;
	}

	private void handleIntent(Intent intent) {
		if (intent == null) {
			// service was restarted
			restartTimers();
		} else {
			Log.d(TAG, "Action: " + intent.getAction());
			final Bundle extras = intent.getExtras();
			if (extras == null) {
				throw new IllegalArgumentException("Must provide extras with timer information set");
			}

			int timerId = extras.getInt(EXTRA_TIMER_ID, timerIdGenerator.incrementAndGet());
			Timer timer = timers.get(timerId);

			// timer will be null if the service has been expunged but the notification not removed,
			// or if there is lag between clicking and the service being killed, etc.
			if (ACTION_START_TIMER.equals(intent.getAction())) {
				start(extras, timerId);
			} else if (ACTION_DO_NOTHING.equals(intent.getAction())) {
				// just do nothing. happens when people click the ongoing notification.
				// this is for honeycomb and over
				Log.d(TAG, "Doing nothing...");
			} else if (ACTION_SHOW_ACTION_DIALOG.equals(intent.getAction())) {

				// this is for gingerbread. In honeycomb we can manipulate the buttons directly.
				showTimerControlDialog(timer);
			} else if (ACTION_PAUSE_TIMER.equals(intent.getAction())) {
				if (timer != null) {
					timer.pause();
					updateOngoingNotifications();
				}
			} else if (ACTION_RESUME_TIMER.equals(intent.getAction())) {
				if (timer != null) {
					timer.resume();
					updateOngoingNotifications();
				}
			} else if (ACTION_STOP_TIMER.equals(intent.getAction())) {
				stop(timerId, true);
			} else if (ACTION_DELETE_TIMER.equals(intent.getAction())) {
				int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
				if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
					// we don't actually have the timer id in this intent, so we're going to have
					// to do a best-effort to resolve this. Mostly likely this will NOT result
					// in a timer id, as it's highly unlikely that people remove widgets while
					// their corresponding timer is running
					Integer potentialTimerId = widgetIdToTimerId.get(widgetId);
					if (potentialTimerId != null) {
						stop(potentialTimerId, true);
					}
					TimerPreferences.delete(this, widgetId);
				}
			} else if (ACTION_REMOVE_NOTIFICATION.equals(intent.getAction())) {
				final NotificationManager notificationService =
						(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				notificationService.cancel(timerId);
				stop(timerId, false);
			}
		}
	}

	private void showTimerControlDialog(final Timer timer) {
		if (timer != null) {
			Intent i = new Intent(this, TimerControl.class);
			i.setAction(TimerControl.ACTION_CONTROL_TIMER);
			i.putExtra(EXTRA_TIMER_ID, timer.getId());
			i.putExtra("paused", timer.isPaused());
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(i);
		}
	}

	private void start(Bundle extras, final int timerId) {
		Timer timer;
		int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
			// this is to support the existing widgets that people may have installed.
			try {
				TimerPreferences timerPreferences = TimerPreferences.get(this, widgetId);
				timer = new Timer(timerId, this, timerPreferences, widgetId);
				widgetIdToTimerId.put(widgetId, timerId);
			} catch (NoSuchTimerException e) {
				Log.e(TAG, "Could not load settings for timer", e);
				return;
			}
		} else {
			int duration = extras.getInt(TeaTimerService.EXTRA_DURATION);
			String teaName = extras.getString(TeaTimerService.EXTRA_TEA_NAME);
			boolean vibrateOnStart = extras.getBoolean(TeaTimerService.EXTRA_VIBRATE_ON_START);
			boolean vibrateOnKitchenTime = extras.getBoolean(TeaTimerService.EXTRA_VIBRATE_ON_KITCHEN_TIME);
			boolean soundAtEnd = extras.getBoolean(TeaTimerService.EXTRA_SOUND_AT_END);
			boolean vibrateAtEnd = extras.getBoolean(TeaTimerService.EXTRA_VIBRATE_AT_END);
			Uri ringtone = null;
			String ringtoneUriAsString = extras.getString(TeaTimerService.EXTRA_RINGTONE);
			if (ringtoneUriAsString != null) {
				ringtone = Uri.parse(ringtoneUriAsString);
			}
			timer = new Timer(timerId,
			                  this,
			                  duration,
			                  teaName,
			                  vibrateOnStart,
			                  vibrateOnKitchenTime,
			                  soundAtEnd,
			                  vibrateAtEnd,
			                  ringtone);
		}
		Log.i(TAG, "Created timer: " + timer);

		start(timer);
	}

	private void start(Timer timer) {
		final int timerId = timer.getId();
		Log.d(TAG, "Starting countdown for timer '" + timerId + "' with duration '" + timer.getDuration() + "': " + timer);
		runningState.start(timerId);
		timers.put(timerId, timer);
		updateOngoingNotifications();
		timer.start();
	}

	private void updateOngoingNotifications() {
		Timer longestRemainingDuration = null;
		for (Timer timer : timers.values()) {
			if (longestRemainingDuration == null || timer.getRemainingDuration() > longestRemainingDuration.getRemainingDuration()) {
				longestRemainingDuration = timer;
			}
		}
		assert longestRemainingDuration != null;
		for (Timer timer : timers.values()) {
			if (timer.getId() != longestRemainingDuration.getId()) {
				updateOngoingNotification(timer);
			}
		}
		startForeground(ONGOING_NOTIFICATION_OFFSET + longestRemainingDuration.getId(),
		                getUpdatedOngoingNotification(longestRemainingDuration));
	}

	public void updateOngoingNotification(Timer timer) {
		Notification notification = getUpdatedOngoingNotification(timer);
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ONGOING_NOTIFICATION_OFFSET + timer.getId(), notification);
		// _todo: can we show this on the lock screen?
		// this happens automatically on Lollipop or better =)
	}

	private Notification getUpdatedOngoingNotification(Timer timer) {
		String teaName = timer.getTeaName();
		int duration = timer.getRemainingDuration();
		int maxDuration = timer.getDuration();

		RemoteViews notificationUI = new RemoteViews(getPackageName(), R.layout.notification_layout);
		notificationUI.setTextViewText(R.id.tea_name, teaName);
		if (timer.isPaused()) {
			notificationUI.setInt(R.id.pause_resume_button, "setBackgroundResource", android.R.drawable.ic_media_play);
			notificationUI.setOnClickPendingIntent(R.id.pause_resume_button, newPendingIntent(ACTION_RESUME_TIMER, timer));
		} else {
			notificationUI.setInt(R.id.pause_resume_button, "setBackgroundResource", android.R.drawable.ic_media_pause);
			notificationUI.setOnClickPendingIntent(R.id.pause_resume_button, newPendingIntent(ACTION_PAUSE_TIMER, timer));
		}
		notificationUI.setOnClickPendingIntent(R.id.stop_button, newPendingIntent(ACTION_STOP_TIMER, timer));
		if (timer.isPaused()) {
			notificationUI.setTextViewText(R.id.duration, TeaTimer.format(duration) + " (" + getString(R.string.paused) + ")");
		} else {
			notificationUI.setTextViewText(R.id.duration, TeaTimer.format(duration));
		}
		notificationUI.setProgressBar(R.id.progress_bar, maxDuration, duration, false);

		// the "when" is the sorting key for the notifications, so as long as we keep that constant,
		// the ordering between the notifications will be the same. no more switching places.
		// http://stackoverflow.com/questions/6189817/pin-notification-to-top-of-notification-area
		// todo: change this to use the name of the timer?
		Notification notification = new Notification(R.drawable.kteatime, getString(R.string.making_tea), timer.getCreation());
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
			// Honeycomb and higher.
			// here things in the extended view of notifications are clickable.
			// here we don't want to do anything when clicking the whole notification

			notification.contentIntent = newPendingIntent(ACTION_DO_NOTHING, timer);
		} else {
			// Gingerbread and lower.
			// here only the notification itself is clickable.
			// when the user clicks, pop up a dialog to ask what the user wants.
			// pause/resume or stop
			notification.contentIntent = newPendingIntent(ACTION_SHOW_ACTION_DIALOG, timer);
		}

		notification.contentView = notificationUI;

		notification.flags |= Notification.FLAG_ONGOING_EVENT;

		return notification;
	}

	// todo: do we even need this anymore? Is this really a thing?
	private void restartTimers() {
		Log.i(TAG, "Restarting timers");

		/*
		If the service is killed and restarted, create a notification saying:
		"Oops, we were killed by Android. Your tea should have been done X minutes and Y seconds ago."
		Alternatively, if there is still time left, continue the countdown.

		Do this by writing to a preference when we started counting down a certain timerId, and when it's
		supposed to be done.
		 */
		if (!runningState.getTimers().isEmpty()) {
			Log.i(TAG, "Restoring widgets...");
			Toast.makeText(this, R.string.restarted_timers, Toast.LENGTH_LONG).show();
		} else {
			// this should never happen, yet it does
			Log.e(TAG, "We were instructed to restart the timers, even though the running state says we have 0 timers");
		}

		for (String timerIdAsString : runningState.getTimers()) {
			final long previousStartingTimeForThisWidget = runningState.getStartingTime(timerIdAsString);
			if (previousStartingTimeForThisWidget > -1) {
				// this is the result of the service being restarted.
				// thus we need to restart the count down timer (with a reduced duration), or apologize that we ran out of time
				Log.i(TAG, "We know of the start-time for this widget already, it's " + previousStartingTimeForThisWidget);
				// check if we're past where we're supposed to be
				int timerId = Integer.parseInt(timerIdAsString);
				Timer timer = timers.get(timerId);
				if (timer == null) {
					Log.w(TAG,
					      "Restarting tea timer '" + timerIdAsString + "', but no such timer exists. Has presumably been removed");
				} else {

					final long expectedEndTime = previousStartingTimeForThisWidget + (timer.getDuration() * 1000);
					final long timeRemaining = expectedEndTime - System.currentTimeMillis();
					if (timeRemaining < 0) {
						Log.e(TAG,
						      "Sorry, android killed the tea time service. Should have completed for widget '" + timerIdAsString + "' " + (Math
								      .abs(timeRemaining) / 1000) + " ms ago");
						final Uri notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						final Ringtone ringtone = RingtoneManager.getRingtone(this, notificationUri);
						final NotificationManager notificationService =
								(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						Notification notification = new Notification(android.R.drawable.stat_notify_error,
						                                             "TeaTime service killed",
						                                             System.currentTimeMillis());
						if (ringtone != null) {
							notification.sound = notificationUri;
						}
						notification.vibrate = Notifications.VIBRATION_PATTERN;
						String time = DateFormat.getTimeInstance().format(new Date(expectedEndTime));
						final PendingIntent removeNotificationPI = newPendingIntent(ACTION_REMOVE_NOTIFICATION, timer);
						notification.setLatestEventInfo(TeaTimerService.this,
						                                "Sorry, android killed the tea time service.",
						                                "Your tea should have been ready at " + time,
						                                removeNotificationPI);
						notificationService.notify(timerId, notification);
						return;
					} else {
						// there's still time left, start with a shorter duration
						Log.e(TAG,
						      "Restarted with '" + (timeRemaining / 1000) + "' seconds left for widget '" + timerIdAsString + "'");
						timer.setRemainingDuration((int) (timeRemaining / 1000));
						start(timer);
					}
				}
			}
		}
	}

	public void stop(int timerId, boolean cancel) {
		final Timer timer = timers.remove(timerId);
		if (timer != null) {
			if (cancel) {
				timer.stop();
				final NotificationManager notificationService =
						(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				notificationService.cancel(ONGOING_NOTIFICATION_OFFSET + timerId);
			}
			timer.reset();
		}
		runningState.remove(timerId);

		// do this here, so we have the start id BEFORE we check the number of timers.
		// that way, if we get anything in AFTER the timer size was checked,
		// we will call stopSelf() with the wrong startId
		Log.d(TAG, timers.toString());
		int i = lastStartId;
		if (timers.isEmpty()) {
			// no more timers, clean up after us
			Log.i(TAG, "Shutting down service after completion: " + i + ", " + lastStartId);
			stopForeground(true);
			stopSelf(i);
			// todo: ensure that the service actually dies
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "TeaTimerService.onDestroy");
	}

}

