/*
 * Copyright (c) 2012 Markus Jevring <markus@jevring.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package net.jevring.android.teatime;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Arrays;

/**
 * @author markus@jevring.net
 */
public class TeaTimer extends AppWidgetProvider {

	public TeaTimer() {
		Log.d("TeaTimer", "Created new TeaTimer");
	}

	static void initializeWidget(int widgetId, Context context) {
		try {
			final TimerPreferences tp = TimerPreferences.get(context, widgetId);
			Log.d(TeaTimerService.TAG, "Duration for widget " + widgetId + ": " + tp.getDuration());
			updateWidget(context, tp.getTeaName(), tp.getDuration(), tp.getDuration(), widgetId, false, false);
		} catch (NoSuchTimerException e) {
			// todo: this happens when we create a widget and open the widget configurator
			Log.e(TeaTimerService.TAG, "Widget '" + widgetId + "' not found, cannot initialize", e);
		}
	}

	/**
	 * Sets the complete state of the widget. We can't set a partial state anywhere, because screen
	 * rotation will re-use whatever the latest RemoteViews object we used was after rotation.
	 *
	 * @param context     the context in which we're called
	 * @param teaName     the name of the tea
	 * @param duration    the (remaining) duration
	 * @param maxDuration the original duration
	 * @param widgetId    the widget id
	 * @param active      whether we're currently counting down
	 * @param paused      whether the widget is currently paused
	 */
	static void updateWidget(Context context,
	                         String teaName,
	                         int duration,
	                         int maxDuration,
	                         int widgetId,
	                         boolean active,
	                         boolean paused) {
		if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			// only update the widget if it's actually a widget (as opposed to a shortcut)
			return;
		}
		RemoteViews widget = new RemoteViews(context.getPackageName(), R.layout.widget1x1);
		widget.setTextViewText(R.id.tea_name, teaName);

		Intent startTimer = new Intent(context, TeaTimerService.class);
		startTimer.setAction(TeaTimerService.ACTION_START_TIMER);
		startTimer.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

		// passing widgetId as the request code here ensures that each PendingIntent is unique.
		// if we don't do this, we'll end up with the same PendingIntent being registered to all widgets
		PendingIntent startCountDown = PendingIntent.getService(context, widgetId, startTimer, PendingIntent.FLAG_UPDATE_CURRENT);
		// bind it to everything so we don't risk misfires
		widget.setOnClickPendingIntent(R.id.tea_name, startCountDown);
		widget.setOnClickPendingIntent(R.id.widget_layout, startCountDown);
		widget.setOnClickPendingIntent(R.id.progress_bar, startCountDown);
		widget.setOnClickPendingIntent(R.id.duration, startCountDown);

		if (active) {
			widget.setInt(R.id.widget_layout, "setBackgroundResource", R.drawable.appwidget_bg_focused);
		} else {
			widget.setInt(R.id.widget_layout, "setBackgroundResource", R.drawable.appwidget_bg);
		}
		if (paused) {
			widget.setTextViewText(R.id.duration, format(duration) + " (" + context.getString(R.string.paused) + ")");
		} else {
			widget.setTextViewText(R.id.duration, format(duration));
		}

		widget.setProgressBar(R.id.progress_bar, maxDuration, duration, false);
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		if (appWidgetManager.getAppWidgetInfo(widgetId) != null) {
			// this null check should protect us against widgets that have been removed before calling
			// this but after entering the method.
			appWidgetManager.updateAppWidget(widgetId, widget);
		}
	}

	static String format(int duration) {
		int minutes = duration / 60;
		int seconds = duration - (minutes * 60);
		return (minutes == 0 ? "" : minutes + "m ") + (seconds == 0 ? "" : seconds + "s");
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		Log.d("TeaTimer", "TeaTimer.onUpdate: " + Arrays.toString(appWidgetIds));
		for (int appWidgetId : appWidgetIds) {
			initializeWidget(appWidgetId, context);
		}
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		Log.d("TeaTimer", "TeaTimer.onDeleted: appWidgetIds=" + Arrays.toString(appWidgetIds));
		super.onDeleted(context, appWidgetIds);

		for (int appWidgetId : appWidgetIds) {
			Intent stopTimer = new Intent(context, TeaTimerService.class);
			stopTimer.setAction(TeaTimerService.ACTION_DELETE_TIMER);
			stopTimer.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			context.startService(stopTimer);
		}
	}
}
